import Nav from "./Nav";
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from "./ConferenceForm";
import AttendeeForm from "./AttendeeForm";
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import * as React from 'react';



function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
    <Nav />
    <div className="my-5 container">
      <Routes>
      <Route index element={<MainPage />} />
        <Route path="locations">
          <Route path='new' element={<LocationForm/>} />
        </Route>
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm/>}/>
        </Route>
        <Route path="attendees">
          <Route path="new" element={<AttendeeForm/>}/>
        </Route>
        <Route path="attendees">
          <Route path="" element={<AttendeesList attendees={props.attendees}/>}/>
        </Route>
        <Route path="presentation">
          <Route path="new" element={<PresentationForm/>}/>
        </Route>
      </Routes>
    </div>
    </BrowserRouter>
  );
}

export default App;
