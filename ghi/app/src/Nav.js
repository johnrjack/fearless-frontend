import { NavLink } from "react-router-dom";
function Nav(){
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <a className="navbar-brand" href="http://localhost:3001/">Conference GO!</a>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li >
                <NavLink className="nav-link" aria-current="page" to="" >Home</NavLink>
              </li>
              <li >
                <NavLink className="nav-link" aria-current="page" to="/locations/new" >New location</NavLink>
              </li>
              <li >
                <NavLink className="nav-link" aria-current="page" to="/conferences/new" >New conference</NavLink>
              </li>
              <li>
                <NavLink className="nav-link" aria-current="page" to="/presentation/new">New Presentation</NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
}

export default Nav;